import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import {
  useState,
  type FocusEventHandler,
  type PropsWithChildren,
} from 'react';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
  onContactChange?: (contact: IContact) => void;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person,
  sx,
  onContactChange,
}) => {
  const { name, email } = person;
  const handleContactChange = (name: string) => {
    onContactChange({
      ...person,
      name,
    });
  };
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <InlineEditInput onChange={handleContactChange}>
            <Typography variant="subtitle1" lineHeight="1rem">
              {name}
            </Typography>
          </InlineEditInput>
          <Typography variant="caption" color="text.secondary">
            {email}
          </Typography>
        </Box>
      </Box>
    </Card>
  );
};

type IInlineEditInputProps = PropsWithChildren<{
  onChange: (text: string) => void;
}>;
const InlineEditInput = ({ children, onChange }: IInlineEditInputProps) => {
  const [isEditing, setIsEditing] = useState(false);
  function handleEdit() {
    setIsEditing(true);
  }
  console.log(isEditing);

  const handleInputFinishChanging: FocusEventHandler = (e) => {
    // @ts-expect-error eee
    onChange(e.currentTarget.value);
    setIsEditing(false);
  };
  return isEditing ? (
    <input onBlur={handleInputFinishChanging}></input>
  ) : (
    <div onClick={handleEdit}>{children}</div>
  );
};
