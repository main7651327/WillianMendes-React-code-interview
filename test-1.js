function findAddUpIndexes(numbers, target) {
  const map = {};
  for (let i = 0; i < numbers.length; i++) {
    const current = numbers[i];
    const dif = target - current;
    if (map[dif] !== undefined) {
      return [map[dif], i];
    }
    map[current] = i;
  }
}

const testCases = [
  [[2, 7, 11, 15], 9],
  [[3, 2, 4], 6],
  [[3, 3], 6],
  [[0, 3, 4, 7, 5], 8],
];

testCases.forEach((tc) => {
  console.log(findAddUpIndexes(tc[0], tc[1]));
});
// console.log(findAddUpIndexes([3, 4, 6, 1], 10));
